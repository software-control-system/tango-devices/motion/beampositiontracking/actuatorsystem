from conan import ConanFile

class ActuatorSystemRecipe(ConanFile):
    name = "actuatorsystem"
    executable = "ds_ActuatorSystem"
    version = "1.0.7"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Falilou Thiam"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/beampositiontracking/actuatorsystem.git"
    description = "ActuatorSystem project"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "ActuatorPlugins/*", "ActuatorSystem/*", "PluginInterfaces/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
    